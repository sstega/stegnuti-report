package model;

public class LogLine {
	public String message, trace, sceneName;
    public int type, sceneID;
    public long timestamp;
    public String imgName;
}
