package model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Report {
    public String player, arrived, gameVersion, gameName, logName;
	public int errorCount, warningCount;
	
	public Report(Log log, int errors, int warnings, String logName) {
		player = log.player;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm");
		arrived = sdf.format(new Date());
		gameVersion = log.gameVersion;
		this.gameName = log.gameName;
		this.logName = logName;
		errorCount = errors;
		warningCount = warnings;
	}
}
