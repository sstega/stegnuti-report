package rest;


import static spark.Spark.*;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.MultipartConfigElement;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import model.Log;
import model.Report;
import net.lingala.zip4j.ZipFile;

public class Main {
	private static Gson g = new Gson();
	
	private static ArrayList<Report> reports;
	private static HashMap<String, ArrayList<Report>> projectReports;
	private static void SaveReports() {
		File file = new File("static/reports.json");
		try {
			Files.write(file.toPath(), g.toJson(reports).getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void LoadReports() {
		reports = new ArrayList<Report>();
		projectReports = new HashMap<String, ArrayList<Report>>();
		// Load from file if it exists, create empty otherwise
		File file = new File("static/reports.json");
		if (file.isFile()) {
			try {
				reports = new ArrayList<Report>(Arrays.asList(g.fromJson(new String(Files.readAllBytes(file.toPath())), Report[].class)));
				// Populate hashmap
				for (Report r : reports) {
					if (!projectReports.containsKey(r.gameName)) {						
						projectReports.put(r.gameName, new ArrayList<Report>());
					}
					projectReports.get(r.gameName).add(r);
				}
			} catch (JsonSyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private static String getCleanGameName(String gameName) {
		gameName = gameName.replaceAll("[^a-zA-Z0-9]", "");
		return gameName;
	}
	
	
	public static void main(String[] args) {
		// CONFIGURE SPARKJAVA
		port(8080); 
		
		try {
			staticFiles.externalLocation(new File("./static").getCanonicalPath());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		// LOAD DATA FROM DISK
		LoadReports();
		
		
		//
		// POST NEW REPORT
		//
		post("rest/postLog/:fileName", (req, res) -> {
			res.type("application/json");
			
			/* Download zip file
			String filePath = "static/zips/" + req.params("fileName");
			byte[] data = req.bodyAsBytes();
			ByteArrayInputStream bis = new ByteArrayInputStream(data);
			try {
				File file = new File(filePath);
				file.createNewFile();
				Files.write(file.toPath(), data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			String filePath = "static/zips/" + req.params("fileName");
			File file = new File(filePath);

            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));

            try (InputStream input = req.raw().getPart("report").getInputStream()) { // getPart needs to use same "name" as input field in form
                Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
            
			System.out.println("Downloaded file " + filePath);
			
			// Unpack files to their own folder in the temporary directory
			String logName = req.params("fileName").split("\\.")[0];
			// Make the directory
			File ownDir = new File("static/temp/" + logName);
			ownDir.mkdirs();
			new ZipFile(filePath).extractAll(ownDir.toPath().toString());
			
			// Open the log file
			File logFile = new File("static/temp/" + logName + "/" + logName + ".log");
			Log log = g.fromJson(new String(Files.readAllBytes(logFile.toPath())), model.Log.class);
			
			// Clean game name of non alphanumeric chars
			String cleanGameName = getCleanGameName(log.gameName);
			int errors = 0;
			int warnings = 0;
			
			for (model.LogLine line : log.lines) {
				if (line == null) continue;
				if (line.type == 4 || line.type == 0) {
					// Error or exception
					errors++;
				}
				if (line.type == 2) {
					// Warning
					warnings++;
				}
				
				// Image path update
				if (!"".equals(line.imgName)) {
					line.imgName = "logs/" + cleanGameName + "/" + logName + "/" + line.imgName;
				}
			}
			// Overwrite log with updated info
			Files.write(logFile.toPath(), g.toJson(log).getBytes());
			
			// Move log folder from temp to logs
			File src = logFile.getParentFile();
			File dest = new File("static/logs/" + cleanGameName + "/" + logName);
			dest.mkdirs();
			Files.move(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
			
			// Save report
			Report report = new Report(log, errors, warnings, logName);
			reports.add(0, report);
			// Map to project for easier getting
			if (!projectReports.containsKey(report.gameName)) {						
				projectReports.put(report.gameName, new ArrayList<Report>());
			}
			projectReports.get(report.gameName).add(report);
			SaveReports();
			
			return String.format("{ \"game\":\"%s\", \"log\":\"%s\" }", log.gameName, logName);
		});
		
		//
		// GET SPECIFIC REPORT
		//
		get("rest/getReport/:game/:log", (req, res) -> {
			res.type("application/json");

			String game = getCleanGameName(req.params("game"));
			String log = req.params("log");
			
			File file = new File("static/logs/" + game + "/" + log + "/" + log + ".log");
			if (file.isFile())
			{
				// Return json if file is found
				return new String(Files.readAllBytes(file.toPath()));
			} else {				
				res.status(404);
				return "Not found";
			}
		});
		
		//
		// GET PROJECT LIST
		//
		get("rest/projects", (req, res) -> {
			return g.toJson(projectReports.keySet().toArray());
		});
		
		//
		// GET REPORTS FOR PROJECT
		//
		get("rest/reports/:game", (req, res) -> {
			if (!projectReports.containsKey(req.params("game"))) {
				res.status(404);
				return "Project not found!";
			}
			return g.toJson(projectReports.get(req.params("game")));
		});
	}

}
