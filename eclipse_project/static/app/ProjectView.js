Vue.component("projectView", {
    data: function() {
        return {
            reports: [],
            projectName: "undefined"
        }
    },

    template: `
    <div class="row">
        <main role="main" style="position:absolute;left:0;top:0;right:0;bottom:0;">
            <div class="container-fluid">
                <h2 class="text-center mb-3"><b>{{this.projectName}}</b></h2>
                <input type="text" class="form-control mb-2" id="searchInput" placeholder="Search">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Arrived at</th>
                            <th scope="col">Player</th>
                            <th scope="col">Summary</th>
                            <th scope="col">Game version</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="report in reports" @click="viewReport(report)">
                            <td scope="row">{{report.arrived}}</th>
                            <td>{{report.player}}</td>
                            <td>
                                {{report.warningCount}}
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="goldenrod" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                                
                                {{report.errorCount}}
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                            </td>
                            <td>{{report.gameVersion}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </main>
    </div>
    `
    ,

    methods: {
        viewReport: function(report) {
            this.$router.push("/viewReport/" + report.gameName + "/" + report.logName);
        }
    },

    mounted() {
        this.projectName = this.$route.params.projectName;

        axios
        .get("rest/reports/" + this.projectName)
        .then(response => {
            this.reports = response.data;
        })
        .catch(error => { alert(error); });
    }

});