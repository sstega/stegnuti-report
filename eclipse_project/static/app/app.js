const chooseLog = { template : '<chooseLog></chooseLog>' }
const viewReport = { template : '<viewReport></viewReport>' }
const sceneLogs = { template : '<sceneLogs></sceneLogs>' }
const projectView = { template : '<projectView></projectView>' }

const router = new VueRouter({
    mode: 'hash',
    routes: [
        { path : '/', component : chooseLog },
        { path : '/viewReport/:game/:log', component : viewReport },
        { path : '/projectView/:projectName', component : projectView }
    ]
});

var app = new Vue({
    router,
    el: "#stegnutiReport"
});