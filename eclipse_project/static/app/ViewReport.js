Vue.component("viewReport", {
    
    data: function() {
        return {
            log : {},
            sceneFlow : [],
            summary : {},
            sceneLogs : []
        }
    },

    template: `
    <div class="container-fluid">
        <nav class="navbar navbar-light fixed-top flex-md-nowrap p-0 shadow" style="background-color: #e3f2fd;">
            <a :href="'#/projectView/' + log.gameName" class="navbar-brand col-sm-3 col-md-2 mr-0">{{ log.gameName }} v{{log.gameVersion}}</a>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <nav id="sideNav" class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                    <span style="color: gray; font-weight: 500; font-size: large; padding: 10px">    
                        SCENE FLOW
                    </span>
                        <ul class="nav nav-pills flex-column">
                            <li v-for="(scene, index) in sceneFlow" class="nav-item" @mouseenter="sceneMouse(scene, 'enter')" @mouseleave="sceneMouse(scene, 'leave')">
                                <a class="nav-link">
                                    <span style="font-size: medium;">
                                        
                                        <span v-if="scene.showDetails">
                                            <span style="color: lightgray;">
                                                id_{{scene.id}}
                                            </span>
                                            {{scene.name}}
                                            <span v-if="scene.errorCount > 0" style="color: red; font-size: ">
                                                {{scene.errorCount}}
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                            </span>
                                            
                                            <span v-if="scene.warningCount > 0" style="color: goldenrod">
                                                {{scene.warningCount}}
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="goldenrod" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                                            </span>
                                        </span>

                                        <span v-else>
                                            {{scene.name}}
                                        </span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-1" style="position:absolute;left:0;top:0;right:0;bottom:0;padding-top:60px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <div v-for="(obj, index) in sceneLogs" style="padding: 10px;">
                                    <sceneLogs :id="'sl_'+index" :lines="obj.lines" :sceneName="obj.sceneName"></sceneLogs>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
    `
    ,

    methods: {
        sceneMouse : function(scene, type) {
            if (type === "enter") {
                scene.showDetails = true;
            } else {
                scene.showDetails = false;
            }
        }
    },

    mounted() {
        // Pull report from server
        var self = this;
        axios
        .get("rest/getReport/" + self.$route.params.game + "/" + self.$route.params.log)
        .then(response => {
            self.log = response.data;
            presentReport();
        })
        .catch(function(error) { alert(error); });
        
        function presentReport() {
            
            // UTILITY FUNCTIONS ----------------------------------------------------------------------
            function addItemToSummary(type) {
                if (type === "error") {
                    self.sceneFlow[self.sceneFlow.length-1].errorCount++;
                } else {
                    self.sceneFlow[self.sceneFlow.length-1].warningCount++;
                }
            }
            
            String.prototype.pad = function(padStr, len) {
                var str = this;
                while (str.length < len)
                str = padStr + str;
                return str;
            }
            
            function convertTimeStamp(seconds) {
                var date = new Date(seconds*1000);
                var str = date.getHours().toString().pad('0', 2) + ":" + date.getMinutes().toString().pad('0', 2) + ":" + date.getSeconds().toString().pad('0', 2);
                return str;
            }
            
            // ----------------------------------------------------------------------
            // Analyze report and construct representational data
            var currentScene = null;
            for(var i=0; i<self.log.lines.length; i++) {
                // TIMESTAMP CONVERSION
                self.log.lines[i].timestamp = convertTimeStamp(self.log.lines[i].timestamp);
                
                // SCENE FLOW
                if (currentScene !== self.log.lines[i].sceneID) {
                    // This line is from a new scene
                    currentScene = self.log.lines[i].sceneID;
    
                    self.sceneFlow.push({
                        name : self.log.lines[i].sceneName,
                        id : self.log.lines[i].sceneID,
                        warningCount: 0,
                        errorCount: 0,
                        showDetails: false
                    });
                    
    
                    // SCENE LOGS
                    lineObj = self.log.lines[i];
                    lineObj.showDetails = false;
    
                    self.sceneLogs.push({
                        sceneName: self.log.lines[i].sceneName,
                        lines: [lineObj]
                    });
                } else {
                    // This line is from the current scene
                    // SCENE LOGS
                    lineObj = self.log.lines[i];
                    lineObj.showDetails = false;
    
                    self.sceneLogs[self.sceneLogs.length-1].lines.push(lineObj);
                }
                
                // SUMMARY
                if (self.log.lines[i].type === 4 || self.log.lines[i].type === 0) {
                    // Error
                    addItemToSummary("error");
                } else if (self.log.lines[i].type === 2) {
                    // Warning
                    addItemToSummary("warning");
                }
            }
        }
    }

});