﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class LogGenerator : MonoBehaviour
{
    public void PushLog(string message) {
        Debug.Log(message);
    }

    public void PushError(string message) {
        Debug.LogError(message);
    }

    public void PushWarning(string message) {
        Debug.LogWarning(message);
    }

    public void GoToScene(string name) {
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
    }
}
