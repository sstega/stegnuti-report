﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MEC;
using Pixelplacement;
using UnityEngine.Networking;
using System.IO.Compression;

[System.Serializable]
class LogLine {
    public string message, trace, sceneName;
    public int type, sceneID;
    public long timestamp;
    public string imgName;

    public LogLine(string message, string trace, LogType type, string sceneName, int sceneID, long timestamp) 
    {
        this.message = message;
        this.trace = trace;
        this.type = (int) type;
        this.sceneName = sceneName;
        this.sceneID = sceneID;
        this.timestamp = timestamp;
    }

    public LogLine(string message, string trace, LogType type, string sceneName, int sceneID, long timestamp, string imgName) : this(message, trace, type, sceneName, sceneID, timestamp)
    {
        this.imgName = imgName;
    }

}

public class Logger : Singleton<Logger>
{
    [Header("Configuration")]
    [SerializeField]
    private string serverUrl;
    [SerializeField]
    private string playerName;
    [Header("Screenshots")]
    [SerializeField]
    private LogType screenshotOn;
    [SerializeField]
    private bool takeScreenshots;
    [Header("Performance")]
    [SerializeField]
    private float fileWriteInterval = 0.05f; // How often to write one log line to the file in seconds

    private Queue<LogLine> writeQueue;
    private string cur_SceneName;
    private int cur_SceneID;

    private static string reportVersion = "0.1";
    private System.IO.StreamWriter writer;
    private string logsPath;
    private string sessionFolderPath;
    private bool writeIsRunning = false;
    private static bool alreadyActive = false; // Prevents two logger game objects upon return to initial scene

    private bool batchWriteEnabled;
    private void Awake() {
        if (Logger.alreadyActive) {
            Destroy(gameObject);
            return;
        }
        Logger.alreadyActive = true;

        logsPath = Application.persistentDataPath + "/stgrLogs";
        writeQueue = new Queue<LogLine>();


        // Get initial scene params
        cur_SceneName = SceneManager.GetActiveScene().name;
        cur_SceneID = SceneManager.GetActiveScene().buildIndex;

        // Create logs folder if it doesn't exist
        Debug.Log("Logger will save logs at " + logsPath);
        if (!System.IO.Directory.Exists(logsPath))
        {
            System.IO.Directory.CreateDirectory(logsPath);
        }

        StartNewSession();
        
        // Listen to the debug log
        UnityEngine.Application.logMessageReceived += LogCallback;

        // Listen to scene changes
        SceneManager.activeSceneChanged += SceneChangedCallback;
    }

    /// <summary>
    /// Closes current session (if there is one), creates a new folder and starts recording logs there.
    /// </summary>
    public void StartNewSession()
    {
        Debug.Log("Starting new logger session...");

        // Close last session
        CloseSession();

        batchWriteEnabled = true;

        // Create directory for this session's log data
        long timestamp = System.DateTimeOffset.Now.ToUnixTimeSeconds();
        sessionFolderPath = logsPath + "/" + playerName + "_" + timestamp;
        if (!System.IO.Directory.Exists(sessionFolderPath))
        {
            System.IO.Directory.CreateDirectory(sessionFolderPath);
        }

        // Open file stream
        // --> Name is current timestamp
        string logFileName = sessionFolderPath + "/" + playerName + "_" + timestamp + ".log";
        writer = System.IO.File.CreateText(logFileName);

        // Initialize file
        writer.WriteLine("{"); // Json opening char
        writer.WriteLine("\"reportVersion\":\"" + reportVersion + "\",");
        writer.WriteLine("\"gameName\":\"" + Application.productName + "\",");
        writer.WriteLine("\"gameVersion\":\"" + Application.version + "\",");
        writer.WriteLine("\"player\":\"" + playerName + "\",");
        writer.WriteLine("\"lines\":[");

    }

    /// <summary>
    /// Terminates current log file appropriately and closes the filestream.
    /// </summary>
    private void CloseSession()
    {
        if (writer == null || writer.BaseStream == null) return; // session already closed
        batchWriteEnabled = false;
        Debug.Log("Closing logger session...");

        // Flush all lines in queue
        while (writeQueue.Count > 1)
        {
            writer.WriteLine(JsonUtility.ToJson(writeQueue.Dequeue()) + ",");
        }
        writer.WriteLine(JsonUtility.ToJson(writeQueue.Dequeue()));
        writer.WriteLine("]}");
        writer.Close();
    }

    /// <summary>
    /// Writes one log line to the file every writeInterval seconds while there are lines waiting in the queue.
    /// </summary>
    private IEnumerator<float> _WriteBatchCoroutine() {
        writeIsRunning = true;
        // Write headers
        while (batchWriteEnabled) {
            if (writeQueue.Count > 0) {
                // There's something to write
                writer.WriteLine(JsonUtility.ToJson(writeQueue.Dequeue()) + ",");
            }
            else
            {
                // Nothing more to write, stop running the coroutine 
                // until the next log line to preserve resources
                break;
            }

            // Wait until next
            yield return Timing.WaitForSeconds(fileWriteInterval);
        }
        writeIsRunning = false;
    }

    /// <summary>
    /// Keeps track of the current scene.
    /// </summary>
    /// <param name="current">Current Scene</param>
    /// <param name="next">The new scene we are changing to</param>
    private void SceneChangedCallback(Scene current, Scene next) {
        cur_SceneID = next.buildIndex;
        cur_SceneName = next.name;
    }

    /// <summary>
    /// Find first available image name within session directory.
    /// </summary>
    /// <returns>PNG file name in the format 'image{number}.jpg', i.e image1.png.</returns>
    private string GenerateImgName() {
        int counter = 0;
        string prefix = sessionFolderPath + "/image";
        while (System.IO.File.Exists(prefix + counter + ".png")) {
            counter++;
        }
        return "image" + counter + ".png";
    }

    /// <summary>
    /// Called after every log message sent to the Unity Debug system.
    /// </summary>
    /// <param name="condition">The message</param>
    /// <param name="trace">Stack trace where the Debug was called from</param>
    /// <param name="type">Message type, i.e error or warning.</param>
    private void LogCallback(string condition, string trace, LogType type) {
        string imgName = null;
        if (type == screenshotOn && takeScreenshots) {
            imgName = GenerateImgName();
            ScreenCapture.CaptureScreenshot(sessionFolderPath + "/" + imgName);
        }
        writeQueue.Enqueue(new LogLine(condition, trace, type, cur_SceneName, cur_SceneID, System.DateTimeOffset.Now.ToUnixTimeSeconds(), imgName));
        if (!writeIsRunning)
        {
            // Start the write coroutine if it is currently not running
            Timing.RunCoroutine(_WriteBatchCoroutine());
        }
    }

    /// <summary>
    /// Take a screenshot explicitly. Ties the new screenshot with a new log line.
    /// </summary>
    /// <param name="message">Message of the log line</param>
    /// <param name="type">Type of the log line</param>
    public void TakeScreenshot(string message, LogType type) 
    {
        string imgName = GenerateImgName();
        ScreenCapture.CaptureScreenshot(sessionFolderPath + "/" + imgName);
        writeQueue.Enqueue(new LogLine(message, "An explicitly taken screenshot", type, cur_SceneName, cur_SceneID, System.DateTimeOffset.Now.ToUnixTimeSeconds(), imgName));
        if (!writeIsRunning)
        {
            // Start the write coroutine if it is currently not running
            Timing.RunCoroutine(_WriteBatchCoroutine());
        }
    }

    /// <summary>
    /// Closes current session, prepares and sends all logs that exist at logsPath. After each one is successfully sent it is deleted.
    /// Done (kind of) asynchronously through a coroutine.
    /// </summary>
    public void SendLogs()
    {
        CloseSession();
        
        string[] sessionFolders = System.IO.Directory.GetDirectories(logsPath);
        StartCoroutine(_sendLogs(sessionFolders));
    }

    private IEnumerator _sendLogs(string[] sessionFolders)
    {
        int counter = sessionFolders.Length;
        UnityWebRequest www;
        byte[] bytes = null;
        string[] tokens;
        string fileName;
        foreach (string sessionPath in sessionFolders)
        {
            tokens = sessionPath.Split('\\');
            fileName = tokens[tokens.Length-1] + ".stgr";

            try
            {
                // Zip to a .stgr file
                // Delete if file already exists
                if (System.IO.File.Exists(Application.persistentDataPath + "/" + fileName))
                {
                    System.IO.File.Delete(Application.persistentDataPath + "/" + fileName);
                }
                ZipFile.CreateFromDirectory(sessionPath, Application.persistentDataPath + "/" + fileName);

                // Grab the bytes
                bytes = System.IO.File.ReadAllBytes(Application.persistentDataPath + "/" + fileName);

            }
            catch(System.Exception e)
            {
                Debug.LogError("An exception occurred while zipping and reading " + fileName + ": " + e.Message);
            }

            List<IMultipartFormSection> multipartList = new List<IMultipartFormSection>();
            multipartList.Add(new MultipartFormFileSection("report", bytes, fileName, "multipart/form-data"));
            www = UnityWebRequest.Post(serverUrl + "/rest/postLog/" + fileName, multipartList);
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.LogError("Upload of " + fileName + " failed with network error: " + www.error);
            }
            else if (www.responseCode != 200) 
            {
                Debug.LogError(fileName + " upload failed with error code: " + www.responseCode);
            }
            else
            {
                // Success, delete the folder and zip file
                System.IO.Directory.Delete(sessionPath, true);
                System.IO.File.Delete(Application.persistentDataPath + "/" + fileName);
                Debug.Log(fileName + " successfully uploaded, session folder and stgr deleted.");
                counter--;
            }

            yield return new WaitForEndOfFrame();
        }

        if (counter > 0)
        {
            Debug.LogError(counter + "/" + sessionFolders.Length + " reports were not uploaded!");
        }
        else
        {
            Debug.Log("All reports successfully uploaded and local files deleted!");
        }

        StartNewSession();
    }
    private void OnDestroy() {
        if (writer != null) {
            CloseSession();
        }
    }
}
